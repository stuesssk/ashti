#include "server.h"

///COPIED CODE 1////
//SOURCE:
//AUTHOR:
//DECRIPTION: This code was used as an in class example of a
// multiplexing tcp server. The type of server we need for this
// project.

int server(const char *local_host, char port_num[], char *location)
{
    // This function will set up a connection with the requesting client_sz
    // Once a connection is in place, the server will accept valid HTTP
    // packets, parse these packets and send the proper response
    // back to the client machine.

    const char *header200 = "HTTP/1.1 200 OK\nContent-type: text/html\n\n";
    struct addrinfo *results;
    struct addrinfo hints = {0};
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    int err = getaddrinfo(local_host, port_num, &hints, &results);
    if(err != 0) {
        fprintf(stderr, "Could not parse address: %s\n", gai_strerror(err));
        return 2;
    }

    int sd = socket(results->ai_family, results->ai_socktype, 0);
    if(sd < 0) {
        perror("Could not create socket");
        freeaddrinfo(results);
        return 3;
    }

    int set = 1;
    setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &set, sizeof(set));

    err = bind(sd, results->ai_addr, results->ai_addrlen);
    if(err < 0) {
        perror("Could not bind");
        close(sd);
        freeaddrinfo(results);
        return 4;
    }
    freeaddrinfo(results);


    // 5 is the usual backlog as told by Liam Echlin
    err = listen(sd, 5);
    if(err < 0) {
        perror("Could not listen");
        freeaddrinfo(results);
        return 5;
    }
    printf("Program Listening on address ::1 on Port: %d\n", getuid());

    struct sigaction ignorer = {0};
    ignorer.sa_handler = SIG_IGN;
    sigaction(SIGCHLD, &ignorer, NULL);

    ///COPIED CODE 2////
    // SOURCE:In class Example
    // AUTHOR: Liam Echlin
    // Description : sighandler axample used from 
    // in class example: share/network/day03/resurse-search.c
    struct sigaction close_handler = {0};
    close_handler.sa_handler = end_program;
    sigaction(SIGTERM, &close_handler, NULL);
    sigaction(SIGINT, &close_handler, NULL);
    ///END OF COPIED CODE 2////

    // Next 36 lines also pulled from say02/udp_server
    for(;;) {

        struct sockaddr_storage client;
        socklen_t client_sz = sizeof(client);
        char ip[INET6_ADDRSTRLEN];
        unsigned short port;

        int remote = accept(sd, (struct sockaddr *)&client, &client_sz);
        if(remote < 0) {
            perror("Could not accept connection");
            continue;
        }

        // When a new connection comes in spawn a child to handle it
        pid_t child = fork();

        // The child process handles the request
        if(child == 0) {
            close(sd);

            ///COPIED CODE 2////
            // SOURCE:In class Example
            // AUTHOR: Liam Echlin
            // Description : sighandler axample used from 
            // in class example: share/network/day03/resurse-search.c
            struct sigaction close_handler = {0};
            close_handler.sa_handler = end_program;
            sigaction(SIGTERM, &close_handler, NULL);
            sigaction(SIGINT, &close_handler, NULL);
            ///END OF COPIED CODE 2////

            if(client.ss_family == AF_INET6) {
                inet_ntop(client.ss_family,
                        &((struct sockaddr_in6 *)&client)->sin6_addr,
                        ip, sizeof(ip));
                port = ntohs(((struct sockaddr_in6 *)&client)->sin6_port);
            } else {
                inet_ntop(client.ss_family,
                        &((struct sockaddr_in *)&client)->sin_addr,
                        ip, sizeof(ip));
                port = ntohs(((struct sockaddr_in *)&client)->sin_port);
            }
// *************CODE WRITTEN BY SEAN STUESSEL***********************
            // Inform user on server where to setup a connection
            printf("%s:%hu is connected\n", ip, port);

            // Set up variables that each child process needs their
            // own copy of
            buf = calloc(1, PATH_MAX);
            ssize_t received = recv(remote, buf, PATH_MAX - 1, 0);
            int sent;
            char *command;
            char *file;
            char *version;
            static char return_buffer[MAX_BUFF_SIZE];

            while(received > 0){

                // Ensuring buffer is null terminated for parsing
                if(received == PATH_MAX){
                    buf[received - 2] = '\0';
                }else{
                    buf[received - 1] = '\0';
                }
                // Want to keep access to raw input buffer string
                // ERROR checking to print buffer to server stdout
                // printf("%s\n", buf);
                memset(return_buffer, 0, sizeof(return_buffer));

                char *pathname = calloc(1, PATH_MAX);
                FILE *fp;

                
                // Parse buffer for required strings and 
                // ensure null terminated
                command = strtok(buf, " ");
                command[strlen(command)] = '\0';
                file = strtok(NULL, " ");
                file[strlen(file)] = '\0';
                version = strtok(NULL, "\n");
                version[strlen(version)] = '\0';


                // Check to ensure proper http version is being requested
                if(strncasecmp(version, "http/1.1", STR_SIZE) != 0){
                    send_error(remote, 505, location);
                    goto finish_loop;
                } 


                if(strncasecmp(command, "GET", strlen(command)) != 0){
                    send_error(remote, 400, location);

                }else{
                    // If we received a cgi-bin file
                    if(strncasecmp(file, "/cgi-bin", STR_SIZE) == 0){
                        strncpy(pathname, location, strlen(location));
                        strncat(pathname, file, strlen(file));

                        pathname[strlen(pathname) + 1] = '\0';
                        // Ensure user has permission to access file
                        if(access(pathname, R_OK) == -1){
                            send_error(remote, 403, location);
                            goto finish_loop;
                        }
                        // popen handles execution the file and 
                        // setting up the pipe needed to grab the data
                        // that would have gone to stdout
                        fp = popen(pathname, "r");
                        if(!fp){
                            send_error(remote, 404, location);
                            goto finish_loop;
                        }
                        // Sending http 200 header
                        send_http(remote);
                        // Sending all the data caputred by popen
                        while(fgets(return_buffer,MAX_BUFF_SIZE - 1, fp)){
                            sent = send(remote, return_buffer,
                                         strlen(return_buffer), 0);
                            if(sent < 0){
                                perror("Problem sending");
                                send_error(remote, 500, location);
                                // Error sending header, try to get out
                                // an error packt, but just wait for
                                // next request
                                goto finish_loop;
                            }
                        }
                        fclose(fp);
                    // Teapot error 418 Easter Egg
                    }else if(strncasecmp(file, "/teapot", SZ_TEAPOT) == 0){
                        send_error(remote, 418, location);

                    // WWW folder files will start in a /
                    }else if (file[0] == '/' && strlen(file) > 1){
                        // Piece together the absolute path of the file
                        strncpy(pathname, location, strlen(location));
                        strncat(pathname, "/www", 4);
                        strncat(pathname, file, strlen(file));
                        snprintf(return_buffer, PATH_MAX, "%s\n",
                                                              header200);
                        // Ensure null terminated string
                        return_buffer[strlen(return_buffer) + 1] = '\0';

                        pathname[strlen(pathname) + 1] = '\0';

                        // Ensure user has permission to access file
                        if(access(pathname, R_OK) == -1){
                            send_error(remote, 403, location);
                            goto finish_loop;
                        }
                        // Open requested file and send the contents
                        fp = fopen(pathname, "r");
                        sent = send(remote, return_buffer,
                                         strlen(return_buffer) + 1, 0);
                        if(sent < 0){
                            perror("Problem sending");
                            send_error(remote, 500, location);
                            // Error sending header, try to get out
                            // an error packt, but just wait for
                            // next request
                            goto finish_loop;
                        }
                        while(!feof(fp)){
                            fgets(return_buffer,sizeof(return_buffer), fp);
                            sent = send(remote, return_buffer,
                                         strlen(return_buffer) + 1, 0);
                            if(sent < 0){
                                perror("Problem sending");
                                send_error(remote, 500, location);
                                // Error sending header, try to get out
                                // an error packt, but just wait for
                                // next request
                                goto finish_loop;
                            }
                        }
                        fclose(fp);
                    }else if (file[0] == '/'){
                        strncpy(pathname, location, strlen(location));
                        strncat(pathname, "/www/index.html", SZ_DEFAULT);
                        // Open requested file and send the contents
                        fp = fopen(pathname, "r");
                        sent = send(remote, return_buffer,
                                         strlen(return_buffer) + 1, 0);
                        if(sent < 0){
                            perror("Problem sending");
                            send_error(remote, 500, location);
                            // Error sending header, try to get out
                            // an error packt, but just wait for
                            // next request
                            goto finish_loop;
                        }
                        while(!feof(fp)){
                            fgets(return_buffer,sizeof(return_buffer), fp);
                            sent = send(remote, return_buffer,
                                         strlen(return_buffer) + 1, 0);
                            if(sent < 0){
                                perror("Problem sending");
                                send_error(remote, 500, location);
                                // Error sending header, try to get out
                                // an error packt, but just wait for
                                // next request
                                goto finish_loop;
                            }
                        }
                        fclose(fp);

                    // Anything else is a page not found
                    }else{
                        send_error(remote, 404, location);
                    }
                }

                finish_loop:
                memset(buf, 0, strlen(buf));
                free(pathname);
                // change size of to just path max
                received = recv(remote, buf, PATH_MAX, 0);
// ************END OF MY CODE*******************************
            }

            close(remote);
            // Valgrind overlords request us to free all our mallocs
            free(buf);
            free(location);
            return 0;
        }
        else if(child < 0) {
            perror("Could not spawn worker");
            continue;
        }

        close(remote);
    }
}

void end_program(int signal)
{
    // Function used in conjunction with the signal handling to 
    // gracefully shutdown the server.

    // Recieved signal that the server is wanting to shutdown
    // Ensure graceful exit
    printf("Program ending, enacting port closing protocol.\n");
    signal = 0;
    if(buf){
        // Valgrind overlords request us to free all our mallocs
        free(buf);
    }
    // Valgrind overlords request us to free all our mallocs
    free(location);
    exit(signal);
}
////END OF COPIED CODE 1////
