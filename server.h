#ifndef SERVER_H
#define SERVER_H

#define _GNU_SOURCE

#include <limits.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <sys/socket.h>
#include <sys/types.h>

#include "error.h"

enum{
    // Setting value for maximum send buffer size
    STR_SIZE = 8,
    SZ_TEAPOT = 7,
    SZ_DEFAULT = 15
};

char *buf;

extern char *location;

// Creates a socket and serves until an interrupting signal is received.
int server(const char *local_host, char port_num[], char *location);

void end_program(int signal);

#endif
