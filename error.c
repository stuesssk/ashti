#include "error.h"


void send_error(int remote, int error, char *pathname)
{
    // Function to build the appropraite error message and send it
    // back to the client.
    static char return_buffer[MAX_BUFF_SIZE];
    snprintf(return_buffer, PATH_MAX, "HTTP/1.1 %d Error\n",
                                          error);
    // build a absolute file path based on the error we are handling
    switch (error){
    case 400:
        strncat(pathname, "/error/400.html", 16);
        break;
    case 403:
        strncat(pathname, "/error/403.html", 16);
        break;
    case 404:
        strncat(pathname, "/error/404.html", 16);
        break;
    case 418:
        strncat(pathname, "/error/418.html", 16);
        break;
    case 505:
        strncat(pathname, "/error/505.html", 16);
        break;
    case 500:
    default:
        strncat(pathname, "/error/500.html", 16);
    }

    // Be sure to null terminate
    pathname[strlen(pathname) + 1] = '\0';

    // Have the absolute path of the file, time to open it
    FILE *file = fopen(pathname, "r");
    if(!error){
        perror("Unable to open server code file!");
    }
    int sent = send(remote, return_buffer,
                     strlen(return_buffer), 0);

    // Send the error html file to the client
    while(fgets(return_buffer,MAX_BUFF_SIZE - 1, file)){
        sent = send(remote, return_buffer,
                     strlen(return_buffer), 0);
        if(sent < 0){
            // No use trying to send a 500 error to client if
            // Server is unable to send original error.
            perror("Unable to send error code");
        }
    }
    fclose(file);
}


void send_http(int remote)
{
    // The default HTTP header to be sent ahead if a good packet is to
    // be sent
    const char *http = "HTTP/1.1 200 OK\n";
    char return_buffer[MAX_BUFF_SIZE];
    snprintf(return_buffer, PATH_MAX, "%s",
                                          http);
    return_buffer[strlen(return_buffer) + 1] = '\0';
    int sent = send(remote, return_buffer,
                     strlen(return_buffer), 0);
    if(sent < 0){
        send_error(remote, 500, location);
    }
}
