CPPFLAGS+=-Wall -Wextra -Wpedantic
CPPFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal
CPPFLAGS+=-Waggregate-return -Winline

CFLAGS+=-std=c11

LDLIBS+=-lm
LDLIBS+=-lssl
LDLIBS+=-lcrypto



BIN=ashti
DEPS=ashti.o error.o server.o

$(BIN) : $(DEPS)

.PHONY: debug profile clean

debug: CFLAGS+=-g
debug: $(BIN)

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: $(BIN)

clean:
	$(RM) $(BIN) $(DEPS)
