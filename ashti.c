///COPIED CODE 1////
//SOURCE:
//AUTHOR:
//DECRIPTION: This code was used as an in class example of a
// multiplexing tcp server. The type of server we need for this
// project.


#include "error.h"
#include "server.h"


char *location;


// The next 39 lines come from the day02/udp_server
int main(int argc, char *argv[])
{
    if(argc != 2) {
        fprintf(stderr, "%s <Top-Level Path>\n", argv[0]);
        return 1;
    }

//***********CODE WRITTEN BY SEAN STUESSEL****************
    /*if(argv[1][strlen(argv[1]) - 1] == '/'){
        argv[1][strlen(argv[1]) - 1] = '\0';
    }*/
    location = calloc(1, PATH_MAX);
    if(!location){
        perror("Unable to Calloc for Top-Level Path");
        return 2;
    }
    // Store top-level path
    realpath(argv[1], location);

    if(access(argv[1], X_OK) == -1) {
        perror("User has insufficent access to Top-Level Path");
        return 3;
    }
    // Argv[1] is top level location for www and cgi bins
//**********************END OF MY CODE********************
    // Port numbers are in the range 1-65535, plus null byte
    char port_num[8];
    const char *local_host = "localhost";
    snprintf(port_num, sizeof(port_num), "%hu", getuid());

    // call server function
    server(local_host, port_num, location);

}
////END OF COPIED CODE 1////
