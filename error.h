#ifndef ERROR_H
#define ERROR_H

#define _GNU_SOURCE

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>


enum{
    // Setting value for maximum send buffer size
    MAX_BUFF_SIZE = 256,
};


extern char *location;

void send_error(int remote, int error, char *pathname);

void send_http(int remote);

#endif
